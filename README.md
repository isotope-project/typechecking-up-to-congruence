# Typechecking up to Congruence Presentation

Assets for my [*Typechecking up to Congruence* presentation at WITS'22](https://popl22.sigplan.org/home/wits-2022#program). Slides available at https://docs.google.com/presentation/d/1nM7UUlEqxAdjZHg73bgHuoC8grAzRLww_U0COMylpm8/edit?usp=sharing